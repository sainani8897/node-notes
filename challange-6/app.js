const notes =  require("./notes.js")
const yargs = require('yargs')
const chalk = require('chalk')


yargs.command({
    command:"add",
    describe:"Add notes to notes.json",
    builder:{
        title:{
            describe:'Its a Title',
            demandOption:true,
            type:"string"

        },
        code:{
            describe:'Its a country code',
            demandOption:true,
            type:"string"
        }
    },
    handler:function (argv) {
        notes.addCountry(argv)
    }

})

yargs.command({
    command:"remove",
    describe:"remove country from countires.json",
    builder:{
        title:{
            describe:'Its a country name',
            demandOption:true,
            type:"string"

        },
    },
    handler:function (argv) {
        notes.removeCountry(argv.title)
    }
})

yargs.command({
    command:"read",
    describe:"read country from countires.json",
    builder:{
        title:{
            describe:'Its a country name',
            demandOption:true,
            type:"string"
        },
    },
    handler:function (argv) {
        notes.readCountry(argv.title)
    }
})
yargs.parse()